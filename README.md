# Bukkit Collection

[![download from GitLab](https://img.shields.io/badge/Download-blue?logo=gitlab)](https://gitlab.com/Programie/BukkitCollection/-/jobs/artifacts/master/raw/target/Collection.jar?job=snapshot-release)

This Minecraft Bukkit plugin provides various small features in a single plugin. Some of them might be moved to a separate plugin in the future.

It is basically a place where features can grow up till they are mature enough to move out to their own plugin.

## Contained features

### ChestClick2Inventory

Enable in config.yml: `ChestClick2Inventory.enabled: true`

Clicking on an inventory block like a chest or barrel will transfer an item from it to the player. Sneaking while clicking on the block transfers a whole stack to the player's inventory.

This feature is enabled as long as the player is not holding a tool or weapon like an axe or sword. For example, it works while holding a block or nothing at all.